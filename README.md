# logger

[![pipeline status](https://gitlab.com/ZulusK/nodejs-logger/badges/master/pipeline.svg)](https://gitlab.com/ZulusK/nodejs-logger/commits/master)
[![coverage report](https://gitlab.com/ZulusK/nodejs-logger/badges/master/coverage.svg)](https://gitlab.com/ZulusK/nodejs-logger/commits/master)

Provides wrapper around [winston@3](https://www.npmjs.com/package/winston) npm module

```bash
npm i zulus/logger
```

### Structure

- [API](#api)
- [Usage](#usage)
- [Contributing rules](#contributing)

<a name="usage"></a>

### USAGE

```js
const Logger = require('@dev/logger');
const config = {
  name: 'my awesome Logger',
  timestamp: true,
  levels: {
    foo: 1
  },
  colors: {
    foo: 'yellow'
  },
  transports: [
    {
      type: 'console',
      level: 'debug'
    },
    {
      type: 'file',
      level: 'debug',
      maxsize: 1024 * 1024 * 10, // 10 MB
      maxFiles: 10,
      filename: 'log.txt'
    }
  ]
};
const logger = new Logger(config);
logger.foo('Hello, %s', 'world');
logger.info("That's all, %O", { who: 'folks' });
```

<a name="api"></a>

### API

### `constructor(config)`

Creates a new Logger

- `config` - configuration of Logger instance
  - `[name]` - name of the logger, will be used as label to each logger output, can be empty
  - `[timestamps]` - is timestamps enabled
  - `[timesatmpsFormat]` - format of timestamps, default `'HH:MM:SS DD/MM/YY'`
  - `[levels]` - custom logging levels, see [example](https://www.npmjs.com/package/winston#using-custom-logging-levels)
  - `[colors]` - custom colors of logger levels, see [example](https://www.npmjs.com/package/winston#using-custom-logging-levels)
  - `transports` - an array of transports to be used for logging, must be not empty
    - `type` - type of transports, one of the next values `console`, `file`
    - `[colorize]` - enable colors in output
    - `[silent]` - make transport silent (disable it)
    - `level` - required, which log level should handle this logger, see [default levels](https://www.npmjs.com/package/winston#logging-levels)
      Next options are used only in file-type logger.
    - `filename` - required in file-type logger, directory + filename of destination log file
    - `[maxsize]` - max file size, if size of log file will be greater than this value, winston will create a new one
    - `[maxFiles]` - max number of log files, old files will be replaced by new files

### `patchLoggers()`

Replaces all global `console` methods with methods of winston. Eq. call `console.info()` are the same to call `logger.info()`. Original methods are available in property `console._native`. So you don't need to import logger in each your file. You can use global defined object `console`

<a name="contributing"></a>

### Contributing

To start contributing do

```bash
git clone git@gitlab.com:ZulusK/nodejs-logger.git
git checkout develop
git checkout -b <your-branch-name>
```

The project is developed in accordance with the [GitFlow][gitflow-docs] methodology.

##### What it means

1. All work you should do in your own **local** branch (naming is important, look below), then make pull request to **develop** branch
2. Your local branch should not have conflicts with repository **develop** branch. To avoid it, before push to repository, do:
   ```bash
   git pull origin develop
   # resolve all conflicts, if they exists
   git add --all
   git commit -m "fix conflicts"
   git push origin <your-branch-name>
   ```
3. We use next naming of branches:

| branch template                      | description                                                 |
| ------------------------------------ | ----------------------------------------------------------- |
| `feat/<short-feature-name>`          | new feature, ex. `feat-add-logger`                          |
| `fix/<short-fix-name>`               | fix of existing feature, ex. `fix-logger`                   |
| `refactor/<short-scope-description>` | refactor, linting, style changes, ex. `style-update-eslint` |
| `test/<short-scope-descriptiopn>`    | tests, ex. `test-db-connections`                            |
| `docs/<short-scope-descriptiopn>`    | documentation, ex. `test-db-connections`                    |

##### Important, before push

1. We use **eslint** with this [rules][eslint-rules] to lint code, before making pull
   request, lint your code:
   ```bash
   npm run lint
   ```
2. Before making pull request, run tests

   ```bash
   npm run test
   ```

[gitflow-docs]: https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/
[eslint-rules]: .eslintrc
[docs-dir]: docs/overview.md
