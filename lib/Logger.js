const winston = require('winston');
const fs = require('fs');
const path = require('path');

const formatError = errObj => errObj.stack || errObj.message;
const DEFAULT_TIMESTAMP_FORMAT = 'HH:MM:SS DD/MM/YY';
const CONSOLE_TRANSPORT = 'console';
const FILE_TRANSPORT = 'file';

const customPrintf = winston.format.printf(
  info => {
    if (info instanceof Error) {
      return `${info.timestamp || ''}[${info.label}] ${info.level}: ${formatError(info)}`;
    } else if (info.message instanceof Error) {
      return `${info.timestamp || ''}[${info.label}] ${info.level}: ${formatError(info.message)}`;
    } else {
      return `${info.timestamp || ''}[${info.label}] ${info.level}: ${info.message}`;
    }
  }
);

/**
 * Wrapper around winston@3 library, provides opportunity to build flexible logger
 */
class Logger {
  /**
   * create a new Logger instance
   * @param config
   */
  constructor(config) {
    this.name = config.name;
    this.timestamps = config.timestamps;
    this.timestampsFormat = config.timestampsFormat || DEFAULT_TIMESTAMP_FORMAT;
    this.levels = Object.assign(winston.config.syslog.levels, config.levels || {});
    this.colors = Object.assign(winston.config.syslog.colors, config.colors || {});
    this.logger = winston.createLogger({
      exitOnError: false,
      levels: this.levels,
      transports: this._buildTransports(config)
    });
    this._applyLogLevels();
    winston.addColors(this.colors);
  }

  _applyLogLevels() {
    for (const level in this.levels) {
      this[level] = this.logger[level];
    }
  }

  _buildFormats(config) {
    const formats = [
      winston.format.splat(),
    ];
    if (config.type !== FILE_TRANSPORT) {
      formats.push(winston.format.colorize());
    }
    if (this.name) {
      formats.push(winston.format.label({ label: this.name }));
    }
    if (this.timestamps || config.timestamps) {
      formats.push(winston.format.timestamp({ format: this.timestampsFormat || config.timestampsFormat }));
    }
    formats.push(customPrintf);
    return winston.format.combine(...formats);
  }

  _buildTransports(config) {
    return config.transports.map(transport => {
      const opts = {
        ...transport,
        format: this._buildFormats(transport)
      };
      switch (transport.type) {
        case FILE_TRANSPORT:
          const dirname = path.dirname(transport.filename);
          // Create the directory if it does not exist
          if (!fs.existsSync(dirname)) {
            fs.mkdirSync(dirname);
          }
          return new winston.transports.File(opts);
        case CONSOLE_TRANSPORT:
        default:
          return new winston.transports.Console(opts);
      }
    });
  }

  /**
   * Replaces all global console methods with methods of this logger.
   */
  patchLoggers() {
    console._native = {
      log: console.log,
      error: console.error,
      debug: console.debug
    };
    Object.keys(this.levels).forEach(level => {
      console[level] = this.logger[level];
    });
  }
}

module.exports = Logger;
