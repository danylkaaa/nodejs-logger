const { expect } = require('chai');
const Logger = require('../lib');

describe('Logger', () => {
  it('should not throw an error when create an instance of Logger', async () => {
    expect(() => new Logger({
      levels: {
        foo: 1
      },
      colors: {
        foo: 'yellow'
      },
      transports: [{
        type: 'console',
        level: 'debug',
      }]
    })).to.not.throw();
  });
  it('replaces console.log after patchLoggers', async () => {
    const logger = new Logger({
      levels: {
        foo: 1
      },
      colors: {
        foo: 'yellow'
      },
      transports: [{
        type: 'console',
        level: 'debug',
      }]
    });
    logger.patchLoggers();
    expect(console.foo).to.eq(logger.foo);
  });
  it('should not throw an error when call .info', async () => {
    const logger = new Logger({
      levels: {
        foo: 1
      },
      colors: {
        foo: 'yellow'
      },
      transports: [{
        type: 'console',
        level: 'debug',
      }]
    });
    logger.patchLoggers();

    expect(() => console.foo('hello world')).to.not.throw();
  });
});
